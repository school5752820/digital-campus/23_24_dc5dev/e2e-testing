import { describe, it } from "vitest";
import {renderList} from "./render_list.js";

describe('render list', () => {
    it('should render a list of users', async ({ expect }) => {
        const users = [
            {name: 'Alice', age: 25, id: 1, lastname: 'Smith'},
            {name: 'Bob', age: 30, id: 2, lastname: 'Doe'},
            {name: 'Charlie', age: 35, id: 3, lastname: 'Brown'},
            {name: 'David', age: 40, id: 4, lastname: 'Johnson'},
        ];

        await renderList(users);

        const ul = document.querySelector('ul');

        expect(ul).not.toBeNull();
        expect(ul).toBeInstanceOf(HTMLUListElement);

        const lis = ul ? [...ul.querySelectorAll('li')] : [];

        expect(lis).toHaveLength(users.length);
    });

    it.only('should render correct informations about a user', async ({ expect }) => {
        const users = [
            {name: 'Alice', age: 25, id: 1, lastname: 'Smith'},
        ];

        await renderList(users);

        const ul = document.querySelector('ul');
        const lis = ul ? [...ul.querySelectorAll('li')] : [];

        expect(lis).toHaveLength(users.length);

        const li = lis[0];
        const user = users[0];

        expect(li).not.toBeNull();
        expect(li).toBeInstanceOf(HTMLLIElement);

        const paragraph = li.querySelector<HTMLParagraphElement>('p:first-child');
        const name = li.querySelector<HTMLElement>('p strong');
        const age = li.querySelector<HTMLParagraphElement>('p:last-child');

        expect(name).not.toBeNull();
        expect(name).toBeInstanceOf(HTMLElement);
        expect(name?.tagName).toBe('STRONG');
        expect(name?.textContent).toBe(`${user.name} ${user.lastname}`);

        expect(age).not.toBeNull();
        expect(age).toBeInstanceOf(HTMLParagraphElement);
        expect(age?.textContent).toBe(`${user.age} ans`);

        expect(paragraph?.dataset.id).toBe(user.id.toString());
    });
});
