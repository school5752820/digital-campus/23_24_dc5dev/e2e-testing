import './style.css';
import ky from "ky";

type User = {
    id: number;
    name: string;
    lastname: string;
    age: number;
};

export async function getUsers() {
  const response = await ky.get('//localhost:3000/users');
  return response.json<User[]>();
}

export async function renderList(users: User[]) {
  const usersList = document.createElement('ul');
  usersList.dataset.testid = 'users-list';
  users.forEach(user => {
    const li = document.createElement('li');
    li.innerHTML = `<p data-id="${user.id}"><strong>${user.name} ${user.lastname}</strong></p>
<p>${user.age} ans</p>`;
    usersList.appendChild(li);
  });

  document.body.appendChild(usersList);
}
