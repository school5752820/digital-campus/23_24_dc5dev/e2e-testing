import { DateTime } from 'luxon';
import {match} from "ts-pattern";

type Period = 'day' | 'week' | 'month' | 'year';

export function getPeriod(startDate: DateTime, period: Period) {
    return match(period)
        .with('day', () => startDate.minus({ days: 1 }))
        .with('week', () => startDate.minus({ weeks: 1 }))
        .with('month', () => startDate.minus({ months: 1 }))
        .with('year', () => startDate.minus({ years: 1 }))
        .exhaustive();
}

export function getLastMonth() {
    const now = DateTime.now();
    return getPeriod(now, 'month');
}

class FetchJokeError extends Error {
    constructor(message: string) {
        super(message);
    }
}

type JokeResponse = {
    categories: string[];
    created_at: string;
    icon_url: string;
    id: string;
    updated_at: string;
    url: string;
    value: string;
}

export async function getJoke(category: string, fetchFn: typeof fetch = fetch) {
    const url = new URL('https://api.chucknorris.io/jokes/random');
    url.searchParams.append('category', category);

    const response = await fetchFn(url.toString());

    if (!response.ok) {
        throw new FetchJokeError('Failed to fetch joke');
    }

    return await response.json() as unknown as Promise<JokeResponse>;
}
