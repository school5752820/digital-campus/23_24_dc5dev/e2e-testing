import {afterEach, beforeEach, describe, it, vi} from 'vitest';
import {getJoke, getLastMonth} from "./smells.ts";
import {DateTime} from "luxon";

describe('Date handling', () => {
    beforeEach(() => {
        vi.useFakeTimers();
        vi.setSystemTime(new Date(2024, 3, 1));
    });

    afterEach(() => {
        vi.useRealTimers();
    });

    it('should render a one month period from 01/04/2024', ({ expect }) => {
        const result = getLastMonth();
        console.log({ result });
        expect(result).toBeInstanceOf(DateTime);
        expect(result.day).toBe(1);
        expect(result.month).toBe(3);
        expect(result.year).toBe(2024);
    });
});

type Fetch = typeof fetch;

function mockPromise<T>() {
    let resolve: (value: T) => void;
    let reject: (error: any) => void;

    const promise = new Promise((res, rej) => {
        resolve = res;
        reject = rej;
    }) as Promise<T> & { resolve: typeof resolve, reject: typeof reject };

    // @ts-ignore
    promise.resolve = resolve;
    // @ts-ignore
    promise.reject = reject;

    return promise;
}

describe('External dependencies handling', () => {
    it.only('should fetch a joke from api', async ({ expect }) => {
        const fetchMock = vi.fn<Parameters<Fetch>, ReturnType<Fetch>>(mockPromise);

        const category = 'dev';
        const resultPromise = getJoke(category, fetchMock);

        fetchMock.mock.results[0].value.resolve(
            new Response(JSON.stringify({
                categories: [category],
                created_at: 'JOKE_CREATED_AT',
                icon_url: 'ICON_URL',
                id: 'JOKE_ID',
                updated_at: 'JOKE_UPDATED_AT',
                url: `https://api.chucknorris.io/jokes/random?category=${category}`,
                value: 'JOKE_MESSAGE'
            }))
        );

        const result = await resultPromise;

        expect(result.url).toBe('https://api.chucknorris.io/jokes/random?category=dev');
        expect(fetchMock).toHaveBeenCalled();
        expect(result).toMatchInlineSnapshot({
            categories: [category],
            created_at: 'JOKE_CREATED_AT',
            icon_url: 'ICON_URL',
            id: 'JOKE_ID',
            updated_at: 'JOKE_UPDATED_AT',
            url: `https://api.chucknorris.io/jokes/random?category=${category}`,
            value: 'JOKE_MESSAGE'
        }, `
          {
            "categories": [
              "dev",
            ],
            "created_at": "JOKE_CREATED_AT",
            "icon_url": "ICON_URL",
            "id": "JOKE_ID",
            "updated_at": "JOKE_UPDATED_AT",
            "url": "https://api.chucknorris.io/jokes/random?category=dev",
            "value": "JOKE_MESSAGE",
          }
        `);
    });
});

