import {getUsers, renderList} from "./render_list.ts";

const users = await getUsers();

void renderList(users);