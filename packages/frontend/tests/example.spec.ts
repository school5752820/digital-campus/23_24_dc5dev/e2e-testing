import {expect, Page, test} from '@playwright/test';

test('has title', async ({page}) => {
    const homepage = new HomePage(page);
    await homepage.goto();
    expect(homepage.pageTitle).toBe('E2E testing');
    await expect(homepage.heading).toHaveText("Ma page d'accueil")
    expect(homepage.userList).toBeDefined();
    await expect(homepage.userNames).toHaveText([
        'Alice Smith',
        'Bob Doe',
        'Charlie Brown',
        'David Johnson',
    ]);
});

class HomePage {
    #page: Page;

    constructor(page: Page) {
        this.#page = page;
    }

    async goto() {
        return this.#page.goto('/');
    }

    get pageTitle() {
        return this.#page.title();
    }

    get heading() {
        return this.#page.locator('h1');
    }

    get userList() {
        return this.#page.locator('ul');
    }

    get userNames() {
        return this.#page.locator('li strong');
    }
}
