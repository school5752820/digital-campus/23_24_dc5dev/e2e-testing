import {expect, Page, test} from '@playwright/test';

test('has submitted correct email from form', async ({page}) => {
    const formPage = new FormPage(page);

    await formPage.goto();
    await formPage.input.fill('test@testeu.tes');
    await page.keyboard.press('Meta+A');
    await formPage.input.fill('test@test.test');
    await formPage.submit();

    const url = new URL(page.url());
    expect(url.searchParams.get('email')).toBe('test@test.test');
});

class FormPage {
    #page: Page;

    constructor(page: Page) {
        this.#page = page;
    }

    async goto() {
        return this.#page.goto('/form');
    }

    get input() {
        return this.#page.locator('input[type=email]');
    }

    get button() {
        return this.#page.locator('button[type=submit]');
    }

    submit() {
        return this.button.click();
    }
}
