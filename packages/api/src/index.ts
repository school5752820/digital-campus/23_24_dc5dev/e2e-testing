import {serve} from '@hono/node-server'
import {Hono} from 'hono'
import { cors } from 'hono/cors'

const app = new Hono()

app.use('/*', cors());

app.get('/users', (c) => {
    return c.json([
        {name: 'Alice', age: 25, id: 1, lastname: 'Smith'},
        {name: 'Bob', age: 30, id: 2, lastname: 'Doe'},
        {name: 'Charlie', age: 35, id: 3, lastname: 'Brown'},
        {name: 'David', age: 40, id: 4, lastname: 'Johnson'},
    ]);
})

const port = 3000
console.log(`Server is running on port ${port}`)

serve({
    fetch: app.fetch,
    port
})
